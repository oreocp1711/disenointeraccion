import re

class Cadena:
    PI = 3.141592
    def __init__(self, s):
        self.s = s
    def __str__(self) -> str:
        return self.s

    def __del__(self):
        print(f'Se destruye <{self.s}>')

    def __add__(self, otra):
        return self.s + '-' + otra.s

    def __sub__(self, otra):
        pass

    def __mul__(self, otra):
        pass

    def cuentaVocales(self):
        contador = 0
        for letter in self.s.lower():
            if letter in set('aeiouáéíóúAEIOU'):
                contador += 1
        return contador

    def cuentaConsonantes(self):
        counter = 0
        for letter in self.s.lower():
            if letter in set('bcdfghjklmnñpqrstvwxyzBCDFGHJKLMNÑPQRSTVWXYZ'):
                counter += 1
        return counter

    def cuentaMayusculas(self):
        counter = 0
        for letter in range(len(self.s)):
            if self.s[letter].isupper():
                counter += 1
        return counter

    def cuentaPalabras(self):
        if len(self.s) < 1:
            counter = 0
            return counter
        else:
            counter = 1
            for letter in range(len(self.s)):
                if self.s[letter] == " ":
                    counter += 1
            return counter

    def cuentaDiptongos(self):
        counter = 0
        patron = re.compile("[aeiouAEIOU]{2}")
        for i in self.s:
            r = patron.findall(self.s)
        counter = len(r)
        return counter


    

