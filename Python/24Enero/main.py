from cadena import Cadena

miCadena = Cadena("cuidar los arboles")
print(miCadena)
miCadena.cuentaVocales()
print(f'La cadena <{miCadena}> tiene {miCadena.cuentaVocales()} vocales')
print(f'La cadena <{miCadena}> tiene {miCadena.cuentaConsonantes()} consonantes')
print(f'La cadena <{miCadena}> tiene {miCadena.cuentaMayusculas()} mayusculas')
print(f'La cadena <{miCadena}> tiene {miCadena.cuentaPalabras()} palabras')
print(f'La cadena <{miCadena}> tiene {miCadena.cuentaDiptongos()} diptongos')



