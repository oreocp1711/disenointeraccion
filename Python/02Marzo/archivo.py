from cgitb import text
from sys import exit

class Archivo:
    def __init__(self, nombre):
        self.nombre = nombre
        self.lineas = []
        try:
            f = open(nombre, 'r')
            for linea in f:
                self.lineas.append(linea)
            f.close()
        except:
            print(f"No se puede abrir el archivo {nombre}", end='')
            exit()
    def muestra(self):
        contador = 1
        for linea in self.lineas:
            print(f"{contador:0>3}:{linea}")
            contador += 1



    def cuentaLineas(self):
        lineas = len(self.lineas)
        return f'- El numero de lineas es: {lineas}.'
    
    def cuentaVocales(self):
        contador = 0
        for i in range(len(self.lineas)):
            for letter in str(self.lineas[i]).lower():
                if letter in set('aeiou'):
                    contador += 1
        return f'- El numero de vocales es: {contador}.'

    def cuentaConsonantes(self):
        contador = 0
        for i in range(len(self.lineas)):
            for letter in str(self.lineas[i]).lower():
                if letter in set('bcdfghjklmnñpqrstvwxyzBCDFGHJKLMNÑPQRSTVWXYZ'):
                    contador += 1
        return f'- El numero de consonantes es: {contador}.'

    def cuentaPalabras(self):
        archivo = open(self.nombre, 'rt')
        accion = archivo.read()
        datos = accion.split()
        contador = len(datos)
        return f'- El numero de palabras es: {contador}.'
    
    def cuentaMayusculas(self):
        contador = 0
        for linea in range(len(self.lineas)):
            for letras in self.lineas[linea]:
                if letras.isupper():
                    contador += 1
        return f'- El numero de mayusculas es: {contador}.'

    
    def cuentaMinusculas(self):
        contador = 0
        for linea in range(len(self.lineas)):
            for letras in self.lineas[linea]:
                if letras.islower():
                    contador += 1
        return f'- El numero de minusculas es: {contador}.'

    def buscar(self, data):
        estado = False
        contador = 0
        archivo = open(self.nombre, 'rt')
        accion = archivo.read()
        datos = accion.split()
        for palabra in range(len(datos)):
            if datos[palabra] == data:
                estado = True
                contador += 1
        if estado == False:
            return f'- La palabra {data} no se ha encontrado.'
        else:
            return f'- La palabra {data} se ha encontrado :) {contador} veces.'
        
    def __add__(self, file1, file2, newFile):
        print('- Uniendo archivos...')
        archivos = [file1, file2]
        
        # Abre file3 en modo escritura
        with open(newFile, 'w') as outfile:
            # Iteramos
            print('Procesando...')
            for names in archivos:
                # Abrimos cada archivo en modo lectura
                with open(names) as infile:
                    # Leemos datos de archivo1 y archivo2 y escribimos en archivo3
                    outfile.write(infile.read())
                outfile.write("\n")
                print('.........')
        print('- Archivos unidos.')


