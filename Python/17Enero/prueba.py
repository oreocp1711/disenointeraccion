from turtle import *
import turtle
import random



random_colors = ["#0ca90c", "#c20d0d", "#ffffff", "#ffd700", "#3225de", "#8314b9"]

tortuga = turtle.Turtle()
tortuga.speed(0)
tortuga.left(90)

def dibujarArbol(tamanoDeRama, tortuga):
    tortuga.pensize(tamanoDeRama/10)
    if (tamanoDeRama < 40):
        tortuga.dot(random.randint(7, 10), random.choice(random_colors))
        return
    tortuga.forward(tamanoDeRama)
    tortuga.left(45)
    dibujarArbol(tamanoDeRama*0.8, tortuga)
    tortuga.right(90)
    dibujarArbol(tamanoDeRama*0.8, tortuga)
    tortuga.left(45)
    tortuga.backward(tamanoDeRama)

tortuga.penup()
tortuga.setpos(0, -250)
tortuga.pendown()
tortuga.hideturtle()
tortuga.color("#406c35")
dibujarArbol(100, tortuga)


