from turtle import *
import turtle


window = turtle.Screen()
window.bgcolor('#191919')

edison = turtle.Turtle()
edison.speed(0)
edison.hideturtle()

tortuga = turtle.Turtle()

def color(tortuga):
    tortuga.penup()
    tortuga.goto(-500, 0)
    tortuga.pendown()
    tortuga.color("black")
    tortuga.begin_fill()
    for i in range(2):
        tortuga.forward(900)
        tortuga.left(90)
        tortuga.forward(900)
        tortuga.left(90)
    tortuga.end_fill()

arbol = turtle.Turtle()

def casa(edison):
    edison.color('black','#CD5C5C')
    edison.begin_fill()
    edison.left(90)
    for i in range(4):
        edison.forward(160)
        edison.right(90)
    edison.forward(160)
    edison.right(45)
    edison.forward(160)
    edison.right(45)
    edison.forward(160)
    edison.right(90)
    edison.forward(160)
    edison.right(45)
    edison.forward(160)

    edison.goto(160, 160)
    edison.left(180)
    edison.forward(160)

    edison.end_fill()
    edison.penup()

def puerta(edison):
    edison.goto(115, 0)
    edison.pendown()
    edison.color('black', 'brown')
    edison.begin_fill()
    edison.right(315)
    edison.forward(70)
    edison.left(90)
    edison.forward(70)
    edison.left(90)
    edison.forward(70)
    edison.end_fill()
    edison.left(90)
    edison.forward(35)
    edison.left(90)
    edison.forward(70)
    edison.penup()
    
def techo(edison):
    edison.goto(-45, 150)
    edison.pendown()
    edison.color('black', 'orange')
    edison.begin_fill()
    edison.right(90)
    edison.forward(230)
    
    edison.left(43)
    edison.forward(170)

    edison.left(105)
    edison.forward(110)

    edison.left(58)
    edison.forward(150)

    edison.right(347)
    edison.forward(180)
    edison.end_fill()
    edison.penup()




def arboles(arbol, n, x, y):
    for i in range(n):
        arbol.goto(x, y)
        arbol.pendown()
        arbol.color('#228B22')
        arbol.begin_fill()
        for i in range(25):
            arbol.width(25-i)
            arbol.forward(90)
            arbol.left(190)
            arbol.end_fill()
        arbol.penup()


def luna(edison):
    edison.goto(-200, 350)
    edison.pendown()
    edison.color('white')
    edison.begin_fill()
    edison.circle(50) 
    edison.end_fill()   
    edison.pendown()

color(tortuga)
casa(edison)
puerta(edison)
techo(edison)


arboles(edison, 3, -200, -100)
arboles(edison, 3, 300, -100)
arboles(edison, 3, 0, -150)

luna(edison)




window.exitonclick()


