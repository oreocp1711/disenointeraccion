#Miguel Angel Reyes Sanchez
#Marco antonio Amador Roman
#Jesus Emanuel Salgado Lezama 
#Orestes Calvario Palafox
from ast import Str
from sys import exit
import re

#from colorama import Fore, init

class Arch:
    team = [
        "-Miguel Angel Reyes Sanchez",
        "-Marco Anotonio Amador Roman",
        "-Jesus Emanuel Salgado Lezama",
        "-Orestes Calvario Palafox"
    ]
    datos = ""
    def __init__(self, nombre):
        try:
            print(nombre)
            self.f = open(nombre, 'r')
            self.datos = self.f.read().split('\n')
            #print(datos)
        except:
            print("No se puede abrir: ", end="")
            #print(Fore.RED, end='')
            print(f"{nombre}",end='')
            #print(Fore.RESET, end='')
            exit()
        self.nombre = nombre

    def __del__(self):
        try:
            self.f.close()
        except:
            pass

    def muestra(self):
        for linea in self.f:
            print(f'{linea}', end="")
        self.f.seek(0)

    def numeroLineas(self):
        nL = len(self.datos)
        return nL

    def cuentaVocales(self):
        contador = 0
        for i in range(len(self.datos)):
            for letter in str(self.datos[i]).lower():
                if letter in set('aeiouáéíóúAEIOU'):
                    contador += 1
        return contador

    def cuentaConsonantes(self):
        counter = 0
        for i in range(len(self.datos)):
            for letter in str(self.datos[i]).lower():
                if letter in set('bcdfghjklmnñpqrstvwxyzBCDFGHJKLMNÑPQRSTVWXYZ'):
                    counter += 1
        return counter

    def cuentaMayusculas(self):
        counter = 0
        for i in range(len(self.datos)):
            for letter in self.datos[i]:
                if letter.isupper():
                    counter += 1
        return counter

    def cuentaPalabras(self):
        counter = 0

        for palabra in range(len(self.datos)):
            counter += 1
            for letra in self.datos[palabra]:
                #print(letra)
                if letra == ' ':
                    counter += 1
            
        return counter

    def cuentaDiptongos(self):
        counter = 0
        patron = re.compile("[aeiouAEIOU]{2}")
        r = patron.findall(str(self.datos))
        counter = len(r)
        return counter
        
    def equipo(self):
        print()
        for i in range(len(self.team)):
            print(self.team[i])
        print()
        
            


    
"""

ejemplo = Arch('prueba.txt')
ejemplo.muestra()
# Brrrrrrrrr
print("Poner formato")
print(ejemplo.cuentaVocales())
print(ejemplo.numeroLineas())
print(ejemplo.cuentaConsonantes())
print(ejemplo.cuentaMayusculas())
print(ejemplo.cuentaPalabras())
print(ejemplo.cuentaDiptongos())
"""



