from abc import ABC

class MyABC(ABC):
    pass

class sub10:
    pass

class sub20:
    pass

MyABC.register(tuple)
MyABC.register( sub10 )


assert issubclass(tuple, MyABC)
assert issubclass( sub10, MyABC)
assert issubclass( sub20, MyABC), f"no es subclase sub20 de MyABC"
assert isinstance((), MyABC)