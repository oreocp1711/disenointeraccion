class Libro:

    nombre = "juan"

    def __init__(self, numero = 0):
        self.numero = numero

    @staticmethod
    def saludo(nombre):
        print(f"hola {nombre}")

    @classmethod
    def despedida(cls, cuenta):
        return cls(cuenta*2)

Libro.saludo("juan")

uno = Libro(123)
dos = Libro.despedida( 5 )
print(uno.numero)
print(dos.numero)