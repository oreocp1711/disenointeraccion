from sys import exit
from colorama import Fore, init
import re
def rojo( f ):
    def funcion( *args ):
        print(Fore.RED, end='')
        f( *args )
        print()
        print(Fore.RESET)
    return funcion

def azul( f ):
    def funcion( *args ):
        print(Fore.BLUE, end='')
        #print('='*80)
        f( *args )
        print()
        #print('='*80)
        print(Fore.RESET)
    return funcion

class Arch:
    team = [
        "-Miguel Angel Reyes Sanchez",
        "-Marco Anotonio Amador Roman",
        "-Jesus Emanuel Salgado Lezama",
        "-Orestes Calvario Palafox"
    ]
    @rojo
    def __init__(self, nombre):
        # init()
        try:
            f = open(nombre, 'r')
        except:
            print(f'No se puede abrir: {nombre}')
            exit()
        self.nombre = nombre
        self.lineas = []
        for linea in f:
            if linea[-1] =='\n':
                self.lineas.append(linea[:-1])
            else:
                self.lineas.append(linea)
        f.close()

    def muestra(self):
        @rojo
        def par( c, linea):
            print(f'{c:0>3}:{linea}')
        @azul
        def non( c, linea ):
            print(f'{c:0>3}:{linea}')

        contador = 0

        for linea in self.lineas:
            if contador % 2 == 0:
                par(contador, linea)
            else:
                non(contador, linea)
            contador += 1


    def numeroLineas(self):
        return len(self.lineas)

    def cuentaVocales(self):
        def vocales( s ):
            c = 0
            for caracter in s:
                if caracter.lower() in set('aeiouáéíóúü'):
                    c += 1
            return c

        c = 0
        for linea in self.lineas:
            c += vocales( linea )

        return c

    def guarda(self):
        pass

    def __add__(self, file1, file2, newFile):
        try:
            with open(file1, 'r', encoding='utf-8') as f1, open(file2, 'r', encoding='utf-8') as f2, open(newFile, 'w', encoding='utf-8') as f3:
                for l1, l2 in zip(f1, f2):
                    f3.write(l1)
                    f3.write('\n')
                    f3.write(l2)

        except FileNotFoundError:
            print("Uno o ambos archivos no existen o no fueron encontrados")

    def cuentaMayusculas(self):
        counter = 0
        for i in range(len(self.lineas)):
            for letter in self.lineas[i]:
                if letter.isupper():
                    counter += 1
        return counter

    def dump(self):
        pass

    def busca(self, word):
        counter = 0
        patron = re.compile(word)
        r = patron.findall(str(self.lineas))
        counter = len(r)
        return counter



    def equipo(self):
        print()
        for i in range(len(self.team)):
            print(self.team[i])
        print()